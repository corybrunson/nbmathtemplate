Author

Title

Publisher Date

1- to 3-paragraph blurb.
* Introduce author
* Include publisher and date of book

Suggested companion work:
Author, Title

Cory Brunson (he/him) is a Research Assistant Professor at the Laboratory for Systems Medicine at the University of Florida.


Hyperlink URLs:

book on Bookshop.org:

book at publisher website:

author website, if provided:

companion work at publisher website:
