Post-production notes:

- What's on each of the audio files?
- Were any technical settings changed?
- Should parts of the audio be edited or excised (interruptions, flubs, etc.)?
- Where are appropriate moments to break for ads?
