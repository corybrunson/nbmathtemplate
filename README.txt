This directory should contain the following files [with the interviewee's surname in place of "Author"; if multiple, use "Author1,Author2"]:

* `NBMathAuthor-audio-*.mp3` - audio files
* `NBMathAuthor-notes.txt` - notes for post-production
* `NBMathAuthor-blog.txt` - text of and links for the blog post
* `NBMathAuthor-cover.jpg` - book cover image
* `NBMathAuthor-photo(-*).jpg` - author photo(s)
